import React from 'react';
import { Route, Switch } from 'react-router-dom';

import StylesIndex from 'styles/index.module.scss';
import MessageBtn from 'components/MessageBtn';
import Burger from 'components/Navigation/Burger';
import routes from './routes';
import Navigation from '../components/Navigation/Navigation';
import Header from '../components/Header/Header';

const ScrollToTop = () => {
  window.scrollTo(0, 0);
  return null;
};

const Layout = () => (
  <div id="outer-container">
    <Burger />
    <main id="page-wrap">
      <div className={StylesIndex.layout}>
        <Header />
        <Navigation />
        <Switch>
          {routes.map(route => (
            <Route key={route.path} {...route} />
          ))}
        </Switch>
        <MessageBtn />
      </div>
    </main>

    <ScrollToTop />
  </div>
);

export default Layout;
