import About from 'components/Pages/MyVernum/About';
import Accounts from '../components/Pages/Accounts/Accounts';
import Payments from '../components/Pages/Payments/Payments';
import Details from '../components/Pages/Accounts/Details/Details';
import MyVernum from '../components/Pages/MyVernum/MyVernum';
import Cellular from '../components/Pages/Payments/Cellular/Cellular';
import History from '../components/Pages/History/History';
import MyProfile from '../components/Pages/MyProfile/MyProfile';
import Auth from '../components/Pages/Auth/Auth';
import Register from '../components/Pages/Register/Register';
import Sales from '../components/Pages/Sales/Sales';
import Currency from '../components/Pages/Currency/Currency';
import ATM from '../components/Pages/ATM';
import Chat from '../components/Pages/Chat';
import TransferStepper from '../components/Modals/Transfer/TransferStepper/TransferStepper';

export default [
  {
    path: '/auth',
    component: Auth,
    exact: true
  },
  {
    path: '/register',
    component: Register,
    exact: true
  },
  {
    path: '/',
    component: MyVernum,
    exact: true
  },
  {
    path: '/accounts',
    component: Accounts,
    exact: true
  },
  {
    path: '/accounts/details',
    component: Details,
    exact: true
  },
  {
    path: '/payments',
    component: Payments,
    exact: true
  },
  {
    path: '/payments/cellular',
    component: Cellular,
    exact: true
  },
  {
    path: '/payments/transfer',
    component: TransferStepper,
    exact: true
  },
  {
    path: '/history',
    component: History,
    exact: true
  },
  {
    path: '/profile',
    component: MyProfile,
    exact: true
  },
  {
    path: '/vernum',
    component: MyVernum,
    exact: true
  },
  {
    path: '/vernum/about',
    component: About,
    exact: true
  },
  {
    path: '/sales',
    component: Sales,
    exact: true
  },
  {
    path: '/currency',
    component: Currency,
    exact: true
  },
  {
    path: '/atm',
    component: ATM,
    exact: true
  },
  {
    path: '/chat',
    component: Chat,
    exact: true
  }
];
