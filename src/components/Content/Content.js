import React from 'react';

import Styles from './Content.module.scss';

const Content = () => <div className={Styles.content} />;

export default Content;
