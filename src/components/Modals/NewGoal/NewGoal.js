import React, { Component } from 'react';
import Modal from 'react-modal';
import InputRange from 'react-input-range';

import 'react-input-range/lib/css/index.css';

import Styles from './NewGoal.module.scss';

class NewGoal extends Component {
  constructor() {
    super();

    this.state = {
      modalIsOpen: false,
      credit: 40000,
      month: 36
    };

    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal() {
    this.setState({ modalIsOpen: true });
  }

  afterOpenModal() {}

  closeModal() {
    this.setState({ modalIsOpen: false });
  }

  render() {
    return (
      <div className={Styles.credit}>
        <button className={Styles.make_task} onClick={this.openModal}>
          Создать цель
        </button>
        <Modal
          className={Styles.modal}
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          contentLabel="Example Modal"
          overlayClassName={Styles.overlay}
        >
          <div className={Styles.modal_wrap}>
            <div className={Styles.title}>Оформление кредита наличными</div>
            <div className={Styles.steps}>
              <form action="" className={Styles.form}>
                <div className={Styles.step}>
                  <div className={Styles.number}>1</div>
                  <div className={Styles.field}>
                    <div className={Styles.label}>Ваш ежемесячный доход</div>
                    <div className={Styles.input}>
                      <input
                        type="text"
                        placeholder="Введите Ваш ежемесячный доход"
                      />
                      <span className={Styles.uah_sign}>₴</span>
                    </div>
                  </div>
                </div>

                <div className={Styles.step}>
                  <div className={Styles.number}>2</div>
                  <div className={Styles.field}>
                    <div className={Styles.label}>Желаемая сума кредита</div>
                    <div className={Styles.range}>
                      <InputRange
                        maxValue={100000}
                        minValue={5000}
                        value={this.state.credit}
                        onChange={credit => this.setState({ credit })}
                      />
                    </div>
                  </div>
                </div>

                <div className={Styles.step}>
                  <div className={Styles.number}>3</div>
                  <div className={Styles.field}>
                    <div className={Styles.label}>Срок кредита</div>
                    <div className={Styles.range}>
                      <InputRange
                        maxValue={60}
                        minValue={6}
                        value={this.state.month}
                        onChange={month => this.setState({ month })}
                      />
                    </div>
                  </div>
                </div>

                <div className={Styles.info}>
                  <div className={Styles.payment}>
                    Ежемесячный платеж:
                    <div className={Styles.value}>
                      {this.state.credit}
                      <span className={Styles.uah_sign}>₴</span>
                    </div>
                  </div>

                  <div className={Styles.desc}>
                    <p>
                      *Разрешение о выдаче и сумме кредита принимается по
                      усмотрению банка. Тарифный план будет определен на
                      основании индивидульной оценки заемщика
                    </p>
                  </div>
                </div>

                <div className={Styles.buttons}>
                  <button onClick={this.closeModal} className={Styles.close}>
                    Отмена
                  </button>
                  <button type="submit" className={Styles.submit}>
                    Продолжить
                  </button>
                </div>
              </form>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

export default NewGoal;
