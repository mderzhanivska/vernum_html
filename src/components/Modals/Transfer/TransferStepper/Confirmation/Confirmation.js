import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Styles from './Confirmation.module.scss';

class Confirmation extends Component {
  render() {
    return (
      <section className={Styles.confirmation}>
        <div className={Styles.info}>
          <div className={Styles.info_block}>
            <div className={Styles.label}>Карта для оплаты</div>
            <div className={Styles.data}>Григоров В.С. **** 4883</div>
          </div>

          <div className={Styles.info_block}>
            <div className={Styles.label}>Всего к оплате:</div>
            <div className={Styles.data}>1001, 00 ₴</div>
          </div>
        </div>
        <div className={Styles.terms}>
          <div className={Styles.label}>
            Нажимая кнопку "Продолжить" Вы принимаете
          </div>
          <Link to="/" className={Styles.link}>
            Пользовательское соглашение
          </Link>
        </div>
      </section>
    );
  }
}

export default Confirmation;
