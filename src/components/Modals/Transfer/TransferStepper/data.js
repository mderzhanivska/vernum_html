import StepOne from 'images/transfer_step1_blue.png';
import StepTwo from 'images/transfer_step2_blue.png';
import StepThree from 'images/transfer_step3_blue.png';

import data from './Data/index';
import confirmation from './Confirmation/index';
import payment from './Payment/index';

export default [
  {
    title: 'Данные платежа',
    icon: StepOne,
    completedIcon: StepOne,
    component: data
  },
  {
    title: 'Подтверждение',
    icon: StepTwo,
    completedIcon: StepTwo,
    component: confirmation
  },
  {
    title: 'Оплата',
    icon: StepThree,
    completedIcon: StepThree,
    component: payment
  }
];
