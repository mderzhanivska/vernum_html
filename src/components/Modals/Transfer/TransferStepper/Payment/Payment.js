import React, { Component } from 'react';

import Styles from './Payment.module.scss';
import { Link } from 'react-router-dom';

class Payment extends Component {
  render() {
    return (
      <section className={Styles.payment}>
        <div className={Styles.info}>
          <div className={Styles.info_block}>
            <div className={Styles.label}>Готово!</div>
            <div className={Styles.data}>Операция прошла успешно</div>
            <div className={Styles.label}>Ваш платеж принят в обработку</div>
          </div>
        </div>
        <div className={Styles.save}>
          <Link to="/" className={Styles.link}>
            Сохранить как шаблон
          </Link>
          <Link to="/" className={Styles.link}>
            Сохранить в календарь
          </Link>
        </div>
      </section>
    );
  }
}

export default Payment;
