import React, { Component } from 'react';

import Card from 'images/card-black.png';
import CardTwo from 'images/card-white.png';
import StylesIndex from 'styles/index.module.scss';
import Styles from './Data.module.scss';
import { Checkbox, Form } from 'semantic-ui-react';

class Data extends Component {
  state = {};
  handleChange = (e, { value }) => this.setState({ value });

  render() {
    return (
      <section className={Styles.data}>
        <div className={StylesIndex.container}>
          <div className={StylesIndex.row}>
            <div className={Styles.data_block}>
              <div className={Styles.image}>
                <img src={Card} alt="" />
              </div>
              <div className={Styles.info}>
                <Form className={Styles.form}>
                  <div className={Styles.title}>С карты</div>
                  <div className={Styles.cards}>
                    <div className={Styles.label}>Выберите карту</div>
                    <div className={Styles.card}>
                      <Checkbox
                        radio
                        className={Styles.checkbox}
                        label="****9058 Карта MasterCard"
                        name="checkboxRadioGroup"
                        value="one"
                        checked={this.state.value === 'one'}
                        onChange={this.handleChange}
                      />
                      <div className={Styles.amount}>17 703, 00 $</div>
                    </div>

                    <div className={Styles.card}>
                      <Checkbox
                        radio
                        className={Styles.checkbox}
                        label="****3589 MasterCard Standart"
                        name="checkboxRadioGroup"
                        value="two"
                        checked={this.state.value === 'two'}
                        onChange={this.handleChange}
                      />
                      <div className={Styles.amount}>17 703, 00 $</div>
                    </div>

                    <div className={Styles.card}>
                      <Checkbox
                        radio
                        className={Styles.checkbox}
                        label="****6804 Кредитный счет"
                        name="checkboxRadioGroup"
                        value="three"
                        checked={this.state.value === 'three'}
                        onChange={this.handleChange}
                      />
                      <div className={Styles.amount}>17 703, 00 $</div>
                    </div>
                  </div>
                </Form>
              </div>
            </div>

            <div className={Styles.data_block}>
              <div className={Styles.image}>
                <img src={CardTwo} alt="" />
              </div>
              <div className={Styles.info}>
                <form className={Styles.form}>
                  <div className={Styles.title}>На карту</div>
                  <div className={Styles.cards}>
                    <div className={Styles.field}>
                      <div className={Styles.label}>Номер карты</div>
                      <input
                        type="text"
                        className={Styles.number}
                        placeholder="0000 0000 0000 0000"
                      />
                    </div>
                    <div className={Styles.field}>
                      <div className={Styles.label}>Сумма</div>
                      <input type="text" className={Styles.sum} />
                      <span> ₴</span>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Data;
