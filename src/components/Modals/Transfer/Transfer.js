import React, { Component } from 'react';
import Modal from 'react-modal';

import Styles from './Transfer.module.scss';
import TransferStepper from './TransferStepper/TransferStepper';

class Transfer extends Component {
  constructor() {
    super();

    this.state = {
      modalIsOpen: false
    };

    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal() {
    this.setState({ modalIsOpen: true });
  }

  afterOpenModal() {}

  closeModal() {
    this.setState({ modalIsOpen: false });
  }

  render() {
    return (
      <div className={Styles.credit}>
        <button className={Styles.make_task} onClick={this.openModal}>
          Перевод
        </button>
        <Modal
          className={Styles.modal}
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          contentLabel="Example Modal"
          overlayClassName={Styles.overlay}
        >
          <div className={Styles.modal_wrap}>
            <TransferStepper />
          </div>
        </Modal>
      </div>
    );
  }
}

export default Transfer;
