import React from 'react';

import { Dropdown } from 'semantic-ui-react';
import Loupe from 'images/loupe.png';

import Menu from 'images/menu.png';

import StylesIndex from 'styles/index.module.scss';
import Styles from './Header.module.scss';
import Search from '../TextField/Search/Search';
import Logo from '../Logo/Logo';

const options = [
  { key: 1, text: 'RU', value: 1 },
  { key: 2, text: 'UKR', value: 2 },
  { key: 3, text: 'EN', value: 3 }
];

const Header = () => (
  <header className={Styles.header}>
    <div className={StylesIndex.container}>
      <div className={Styles.row}>
        <div className={Styles.desktop}>
          <Logo />

          <Search label="Начните вводить текст для поиска" />

          <div className={Styles.newButton}>
            <button type="button">+ Новый продукт</button>
          </div>

          <div className={Styles.languages}>
            <Dropdown
              defaultValue={options[0].value}
              fluid
              compact
              selection
              options={options}
            />
          </div>
        </div>

        <div className={Styles.mobile}>
          <div className={Styles.burger_btn}>
            <button type="button">
              <img src={Menu} alt="" />
            </button>
          </div>
          <div className={Styles.mob_search}>
            <input type="search" placeholder="Поиск" />
            <span className={Styles.mob_loupe}>
              <img src={Loupe} alt="" />
            </span>
          </div>

          <div className={Styles.mob_newButton}>
            <button type="button">+</button>
          </div>

          <div className={Styles.mob_languages}>
            <Dropdown
              defaultValue={options[0].value}
              compact
              selection
              options={options}
            />
          </div>
        </div>
      </div>
    </div>
  </header>
);

export default Header;
