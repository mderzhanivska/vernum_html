import React from 'react';
import { Link } from 'react-router-dom';

import Arrow from 'images/svg/arrow-link.svg';
import Styles from './BackBtn.module.scss';

const BackBtn = () => (
  <div className={Styles.back_btn}>
    <div className={Styles.back}>
      <Link to="/accounts" className={Styles.link}>
        <span className={Styles.arrow}>
          <Arrow />
        </span>
        Назад
      </Link>
    </div>
  </div>
);

export default BackBtn;
