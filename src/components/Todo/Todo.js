import React from 'react';
import { Link } from 'react-router-dom';
import cx from 'classnames';

import ArrowDown from 'images/svg/arrow.svg';
import Icon from 'images/ChangePassword.png';

import Styles from './Todo.module.scss';

const Todo = ({ todos }) => (
  <div className={Styles.todo}>
    <ul className={Styles.todo_list}>
      {todos.map(({ name, link, className }) => (
        <li className={cx(Styles.todo_item, className)}>
          <Link to={link} className={Styles.todoLink}>
            <img src={Icon} alt="" />
            {name}
          </Link>
        </li>
      ))}

      <li className={Styles.show_more}>
        <ArrowDown />
      </li>
    </ul>
  </div>
);

export default Todo;
