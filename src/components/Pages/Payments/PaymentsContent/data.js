import Phone from 'images/mobile.png';
import Card from 'images/card2card.png';
import Transfer from 'images/transfer_banks.png';
import Payments from 'images/communal_payments.png';
import Budget from 'images/budget.png';

export default [
  {
    name: 'Телефонная связь',
    icon: Phone,
    link: '/payments/cellular'
  },
  {
    name: 'Перевод на карту',
    icon: Card,
    link: '/payments/transfer'
  },
  {
    name: 'Банковские переводы',
    icon: Transfer,
    link: '/payments'
  },
  {
    name: 'Коммунальные и интернет',
    icon: Payments,
    link: '/payments'
  },
  {
    name: 'Бюджет и налоги',
    icon: Budget,
    link: '/payments'
  }
];
