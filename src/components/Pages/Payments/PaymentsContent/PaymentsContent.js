import React from 'react';
import { Link } from 'react-router-dom';

import Styles from './PaymentsContent.module.scss';

import payments from './data';

function ListPayments() {
  const listpayments = payments.map(({ icon, name, link }) => (
    <div className={Styles.payment}>
      <Link to={link} className={Styles.link}>
        <img src={icon} className={Styles.image} alt="credit card" />
        <div className={Styles.desc}>{name}</div>
      </Link>
    </div>
  ));
  return <div className={Styles.myPayments}>{listpayments}</div>;
}

const PaymentsContent = () => <ListPayments />;

export default PaymentsContent;
