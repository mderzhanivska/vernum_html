import React from 'react';
import { Tab, TabList, TabPanel, Tabs } from 'react-tabs';
import StylesIndex from 'styles/index.module.scss';
import Styles from './Payments.module.scss';
import PaymentsContent from './PaymentsContent/PaymentsContent';
import MyPaymentsContent from './MyPaymentsContent/MyPaymentsContent';
import Title from '../../Title/Title';

const Payments = () => (
  <section className={Styles.payments}>
    <div className={StylesIndex.container}>
      <div className={StylesIndex.row}>
        <Title title="Платежы" />

        <Tabs className={Styles.tabs}>
          <TabList className={Styles.tablist}>
            <Tab className={Styles.tabitem}>Все платежы</Tab>
            <Tab className={Styles.tabitem}>Мои платежы</Tab>
          </TabList>

          <TabPanel>
            <PaymentsContent />
          </TabPanel>
          <TabPanel>
            <MyPaymentsContent />
          </TabPanel>
        </Tabs>
      </div>
    </div>
  </section>
);

export default Payments;
