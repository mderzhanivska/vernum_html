import React from 'react';
import { Link } from 'react-router-dom';

import Arrow from 'images/svg/arrow-link.svg';
import StylesIndex from 'styles/index.module.scss';
import Styles from './Cellular.module.scss';

import operators from './data';
import Header from '../../../Header/Header';
import Title from '../../../Title/Title';

function ListOperator() {
  const operator = operators.map(({ name, icon }) => (
    <div className={Styles.operator}>
      <Link to="/">
        <div className={Styles.icon}>
          <img src={icon} alt="" />
        </div>
        <div className={Styles.name}>{name}</div>
      </Link>
    </div>
  ));
  return <div className={Styles.operators}>{operator}</div>;
}

const Cellular = () => (
  <section className={Styles.celular}>
    <Header />
    <div className={StylesIndex.container}>
      <div className={StylesIndex.row}>
        <Title title="Платежи и переводы" />

        <div className={Styles.back}>
          <Link to="/payments" className={Styles.link}>
            <span className={Styles.arrow}>
              <Arrow />
            </span>
            Назад
          </Link>
        </div>
        <ListOperator />
      </div>
    </div>
  </section>
);

export default Cellular;
