import Lifecell from 'images/lifecell.png';
import Kyivstar from 'images/kyivstar.png';
import Vodafone from 'images/vodafone.png';
import Mob from 'images/3mob.png';
import MTC from 'images/mtc.png';

export default [
  {
    name: 'Lifecell',
    icon: Lifecell
  },
  {
    name: 'Kyivstar',
    icon: Kyivstar
  },
  {
    name: 'Vodafone',
    icon: Vodafone
  },
  {
    name: '3Mob',
    icon: Mob
  },
  {
    name: 'MTC',
    icon: MTC
  },
  {
    name: 'Lifecell',
    icon: Lifecell
  }
];
