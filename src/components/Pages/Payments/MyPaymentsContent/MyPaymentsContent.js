import React from 'react';
import { Link } from 'react-router-dom';

import Template from 'images/template.png';

import Styles from './MyPaymentsContent.module.scss';

import payments from './data';
import PaymentsContent from '../PaymentsContent/PaymentsContent';

function ListPayments() {
  const listpayments = payments.map(({ name, link }) => (
    <div className={Styles.payment}>
      <Link to={link} className={Styles.link}>
        <img src={Template} className={Styles.image} alt="credit card" />
        <div className={Styles.desc}>{name}</div>
      </Link>
    </div>
  ));
  return <div className={Styles.myPayments}>{listpayments}</div>;
}

const MyPaymentsContent = () => (
  <div className={Styles.myPaymentsWrap}>
    <div className={Styles.title}>Шаблоны</div>
    <ListPayments />
    <div className={Styles.title}>Последние действия</div>
    <PaymentsContent />
  </div>
);

export default MyPaymentsContent;
