import React from 'react';
import cn from 'classnames';

import Styles from './All.module.scss';
import currencies from './data';
import Date from '../Date';

function ListCurrencies() {
  const listCurrencies = currencies.map(({ code, name, buy, sell, nbu }) => (
    <div className={Styles.table_row}>
      <div className={cn(Styles.uah, Styles.big_column)}>
        <div className={Styles.code}>{code}</div>
        <div className={Styles.name}>{name}</div>
      </div>
      <div className={cn(Styles.market, Styles.small_column)}>
        <div className={Styles.price}>{buy}</div>
        <div className={Styles.divider}>/</div>
        <div className={Styles.price}>{sell}</div>
      </div>
      <div className={cn(Styles.nbu, Styles.small_column)}>
        <div className={Styles.price}>{nbu}</div>
      </div>
    </div>
  ));
  return <div className={Styles.table}>{listCurrencies}</div>;
}

const All = () => (
  <section className={Styles.all}>
    <Date />
    <div className={Styles.table}>
      <div className={Styles.table_row}>
        <div className={cn(Styles.title, Styles.big_column)}>Курс к гривне</div>
        <div className={cn(Styles.title, Styles.small_column)}>
          Наличный рынок
          <div className={Styles.label}>Покупка/Продажа</div>
        </div>
        <div className={cn(Styles.title, Styles.small_column)}>НБУ</div>
      </div>
    </div>
    <ListCurrencies />
  </section>
);

export default All;
