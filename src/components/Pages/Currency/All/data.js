export default [
  {
    code: 'USD',
    name: 'доллар США',
    buy: '26,205',
    sell: '26,450',
    nbu: '26,446'
  },
  {
    code: 'EUR',
    name: 'евро',
    buy: '30,255',
    sell: '30,890',
    nbu: '30,503'
  },
  {
    code: 'RUB',
    name: 'российский рубль',
    buy: '0,390',
    sell: '0,420',
    nbu: '0,412'
  },
  {
    code: 'PLN',
    name: 'польский злотый',
    buy: '6,700',
    sell: '7,200',
    nbu: '7,070'
  },
  {
    code: 'GBP',
    name: 'английский фунт стерлингов',
    buy: '33,875',
    sell: '35,000',
    nbu: '34,784'
  },
  {
    code: 'CHF',
    name: 'швейцарский франк',
    buy: '27,750',
    sell: '26,700',
    nbu: '26,522'
  }
];
