import React from 'react';

import Arrow from 'images/arrow_left.png';
import Calendar from 'images/calendar.png';

import Styles from './Date.module.scss';

const Date = () => (
  <section className={Styles.date}>
    <div className={Styles.date_slider}>
      <div className={Styles.label}>Курс валют за</div>
      <div className={Styles.slider}>
        <button className={Styles.arrow}>
          <img src={Arrow} alt="" />
        </button>
        <div className={Styles.text}>Сегодня</div>
        <button className={Styles.arrow}>
          <img src={Arrow} alt="" />
        </button>
      </div>
    </div>

    <div className={Styles.picker}>
      <div className={Styles.label}>Выбрать дату</div>
      <button className={Styles.calendar}>
        <img src={Calendar} alt="" />
      </button>
    </div>
  </section>
);

export default Date;
