import React from 'react';
import cn from 'classnames';

import Styles from './Dollar.module.scss';
import StylesAll from '../All/All.module.scss';
import Date from '../Date';

const Dollar = () => (
  <section className={StylesAll.all}>
    <Date />
    <div className={StylesAll.table}>
      <div className={StylesAll.table_row}>
        <div className={cn(StylesAll.title, Styles.big_column)}>
          Курс к гривне
        </div>
        <div className={cn(StylesAll.title, Styles.small_column)}>Покупка</div>
        <div className={cn(StylesAll.title, Styles.small_column)}>Продажа</div>
      </div>
      <div className={StylesAll.table_row}>
        <div className={cn(StylesAll.desc, Styles.big_column)}>
          Наличный рынок
        </div>
        <div className={cn(StylesAll.market, Styles.small_column)}>
          <div className={StylesAll.price}>26,205</div>
          <div className={StylesAll.growth}>0,085</div>
        </div>
        <div className={cn(StylesAll.market, Styles.small_column)}>
          <div className={StylesAll.price}>26,450</div>
          <div className={StylesAll.growth}>0,085</div>
        </div>
      </div>

      <div className={StylesAll.table_row}>
        <div className={cn(StylesAll.desc, Styles.big_column)}>НБУ</div>
        <div className={cn(StylesAll.market, Styles.small_column)}>
          <div className={StylesAll.price}>26,450</div>
          <div className={StylesAll.growth}>0,033</div>
        </div>
        <div className={cn(StylesAll.market, Styles.small_column)}>
          <div className={StylesAll.price}>26,450</div>
          <div className={StylesAll.growth}>0,033</div>
        </div>
      </div>
    </div>
  </section>
);

export default Dollar;
