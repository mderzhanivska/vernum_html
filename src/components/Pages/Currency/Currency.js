import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import StylesIndex from 'styles/index.module.scss';
import Styles from './Currency.module.scss';

import All from './All';
import Dollar from './Dollar';
import Title from '../../Title/Title';

const Currency = () => (
  <section className={Styles.currency}>
    <div className={StylesIndex.container}>
      <div className={StylesIndex.row}>
        <Title title="Курс валют" />

        <Tabs className={Styles.tabs}>
          <TabList className={Styles.tablist}>
            <Tab className={Styles.tabitem}>Все валюты</Tab>
            <Tab className={Styles.tabitem}>Доллар</Tab>
            <Tab className={Styles.tabitem}>Евро</Tab>
            <Tab className={Styles.tabitem}>Рубль</Tab>
          </TabList>

          <TabPanel>
            <All />
          </TabPanel>
          <TabPanel>
            <Dollar />
          </TabPanel>
          <TabPanel>
            <Dollar />
          </TabPanel>
          <TabPanel>
            <Dollar />
          </TabPanel>
        </Tabs>
      </div>
    </div>
  </section>
);

export default Currency;
