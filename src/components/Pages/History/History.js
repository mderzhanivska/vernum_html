import React from 'react';
import { Link } from 'react-router-dom';
import { Dropdown } from 'semantic-ui-react';

import StylesIndex from 'styles/index.module.scss';
import Styles from './History.module.scss';

import Day from './Day/Day';
import Title from '../../Title/Title';

const options = [
  { key: 1, text: 'Датам', value: 1 },
  { key: 2, text: 'Датам', value: 2 },
  { key: 3, text: 'Датам', value: 3 }
];

const History = () => (
  <section className={Styles.history}>
    <div className={StylesIndex.container}>
      <div className={StylesIndex.row}>
        <Title title="События" />

        <div className={Styles.controls}>
          <div className={Styles.control_block}>
            <div className={Styles.desc}>Операции за весь период</div>
            <div className={Styles.sort}>
              <div className={Styles.sort_desc}>Сортировать по : </div>
              <Dropdown
                defaultValue={options[0].value}
                compact
                selection
                options={options}
              />
            </div>
          </div>
          <div className={Styles.control_block}>
            <Link to="/" className={Styles.set_filter}>
              Настроить фильтр
            </Link>
            <Link to="/" className={Styles.clean_filter}>
              Очистить фильтр
            </Link>
          </div>
        </div>
        <Day />
        <Day />
        <Day />
      </div>
    </div>
  </section>
);

export default History;
