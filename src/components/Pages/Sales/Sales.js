import React from 'react';

import StylesIndex from 'styles/index.module.scss';
import Styles from './Sales.module.scss';

import Title from '../../Title/Title';

const Sales = () => (
  <section className={Styles.sales}>
    <div className={StylesIndex.container}>
      <div className={StylesIndex.row}>
        <Title title="Акции" />
      </div>
    </div>
  </section>
);

export default Sales;
