import React from 'react';

import StylesIndex from 'styles/index.module.scss';
import Styles from './ATM.module.scss';

import Title from '../../Title/Title';

const ATM = () => (
  <section className={Styles.atm}>
    <div className={StylesIndex.container}>
      <div className={StylesIndex.row}>
        <Title title="Отделения и банкоматы" />
      </div>
    </div>
  </section>
);

export default ATM;
