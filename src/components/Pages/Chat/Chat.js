import React from 'react';

import StylesIndex from 'styles/index.module.scss';
import Styles from './Chat.module.scss';

import Title from '../../Title/Title';

const Chat = () => (
  <section className={Styles.chat}>
    <div className={StylesIndex.container}>
      <div className={StylesIndex.row}>
        <Title title="Чат с банком" />
      </div>
    </div>
  </section>
);

export default Chat;
