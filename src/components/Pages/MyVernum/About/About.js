import React from 'react';
import { Link } from 'react-router-dom';

import Arrow from 'images/svg/arrow-link.svg';
import Card from 'images/card-black.png';
import StylesIndex from 'styles/index.module.scss';
import Header from 'components/Header';
import Styles from './About.module.scss';
import Title from '../../../Title/Title';

const About = () => (
  <section className={Styles.about}>
    <Header />
    <div className={StylesIndex.container}>
      <div className={StylesIndex.row}>
        <Title title="Новый продукт" />

        <div className={Styles.back}>
          <Link to="/vernum" className={Styles.link}>
            <span className={Styles.arrow}>
              <Arrow />
            </span>
            Назад
          </Link>
        </div>
        <div className={Styles.card_name}>MasterCard Platinum</div>

        <div className={Styles.card_desc}>
          <div className={Styles.image}>
            <img src={Card} alt="" />
          </div>
          <p>
            Карта MasterCard Platinum - это премиальный продукт, который
            подчеркнет Ваш статус и положение. <br />
            Международная банковская карта категории Platinum гарантирует Вам
            превосходное обслуживание и <br />
            привилегии по всему миру.
          </p>
        </div>

        <div className={Styles.card_pros}>
          <div className={Styles.item}>
            <div className={Styles.percents}>1%</div>
            <p>За любые покупки с карты</p>
          </div>

          <div className={Styles.item}>
            <div className={Styles.countries}>90+</div>
            <p>Используйте карту в 90 странах</p>
          </div>

          <div className={Styles.item}>
            <div className={Styles.currency}>$</div>
            <p>Международная валюта</p>
          </div>

          <div className={Styles.item}>
            <div className={Styles.bonus}>1=1</div>
            <p>1 балл = 1 гривна</p>
          </div>
        </div>

        <div className={Styles.buttons}>
          <div className={Styles.order}>
            <Link to="/">Оформить</Link>
          </div>
          <div className={Styles.another_name}>
            <Link to="/">Заказ на другое имя</Link>
          </div>
          <div className={Styles.apply}>
            <Link to="/">Подать заявку</Link>
          </div>
        </div>
      </div>
    </div>
  </section>
);

export default About;
