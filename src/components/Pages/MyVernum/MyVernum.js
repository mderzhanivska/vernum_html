import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import StylesIndex from 'styles/index.module.scss';
import Styles from './MyVernum.module.scss';
import Individual from './Individual/Individual';
import Title from '../../Title/Title';

const MyVernum = () => (
  <section className={Styles.my_vernum}>
    <div className={StylesIndex.container}>
      <div className={StylesIndex.row}>
        <Title title="Новый продукт" />

        <Tabs className={Styles.tabs}>
          <TabList className={Styles.tablist}>
            <Tab className={Styles.tabitem}>Физлицам</Tab>
            <Tab className={Styles.tabitem}>Бизнесу</Tab>
          </TabList>

          <TabPanel>
            <Individual />
          </TabPanel>
          <TabPanel>
            <Individual />
          </TabPanel>
        </Tabs>
      </div>
    </div>
  </section>
);

export default MyVernum;
