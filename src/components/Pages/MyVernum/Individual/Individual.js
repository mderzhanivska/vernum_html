import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import WhiteCard from 'images/card-white.png';
import BlackCard from 'images/card-black.png';
import Deposit from 'images/deposit.png';

import Styles from './Individual.module.scss';
import NewGoal from '../../../Modals/NewGoal/NewGoal';
import Credit from '../../../Modals/Credit/Credit';

class Individual extends Component {
  constructor() {
    super();

    this.state = {
      modalIsOpen: false
    };

    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal() {
    this.setState({ modalIsOpen: true });
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
    this.subtitle.style.color = '#f00';
  }

  closeModal() {
    this.setState({ modalIsOpen: false });
  }

  render() {
    return (
      <section className={Styles.individual}>
        <div className={Styles.block}>
          <div className={Styles.top}>
            <div className={Styles.title}>Карты</div>
            <div className={Styles.link}>
              <Link to="/">Все карты</Link>
            </div>
          </div>

          <div className={Styles.slider}>
            <div className={Styles.item}>
              <div className={Styles.image}>
                <img src={WhiteCard} alt="" />
              </div>
              <div className={Styles.text}>
                <p className={Styles.desc}>
                  Оплачивайте покупки и услуги в заведениях торговли и сервиса
                </p>
                <div className={Styles.more}>
                  <Link to="/vernum/about">Подробнее</Link>
                </div>
              </div>
            </div>

            <div className={Styles.item}>
              <div className={Styles.image}>
                <img src={BlackCard} alt="" />
              </div>
              <div className={Styles.text}>
                <p className={Styles.desc}>
                  Оплачивайте покупки и услуги в заведениях торговли и сервиса
                </p>
                <div className={Styles.more}>
                  <Link to="/vernum/about">Подробнее</Link>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className={Styles.block}>
          <div className={Styles.top}>
            <div className={Styles.title}>Депозиты</div>
            <div className={Styles.link}>
              <Link to="/vernum/about">Все депозиты</Link>
            </div>
          </div>

          <div className={Styles.slider}>
            <div className={Styles.item}>
              <div className={Styles.image}>
                <img src={Deposit} alt="" />
              </div>
              <div className={Styles.text}>
                <p className={Styles.desc}>
                  Оплачивайте покупки и услуги в заведениях торговли и сервиса
                </p>
                <div className={Styles.more}>
                  <Link to="/vernum/about">Подробнее</Link>
                </div>
              </div>
            </div>

            <div className={Styles.item}>
              <div className={Styles.image}>
                <img src={Deposit} alt="" />
              </div>
              <div className={Styles.text}>
                <p className={Styles.desc}>
                  Оплачивайте покупки и услуги в заведениях торговли и сервиса
                </p>
                <div className={Styles.more}>
                  <Link to="/">Подробнее</Link>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/*<div className={Styles.buttons}>*/}
        {/*<NewGoal />*/}
        {/*<Credit />*/}
        {/*</div>*/}
      </section>
    );
  }
}

export default Individual;
