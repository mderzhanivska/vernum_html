import React from 'react';

import card from 'images/card-black.png';
import StylesIndex from 'styles/index.module.scss';
import Styles from './Details.module.scss';

import general from './data';
import Title from '../../../Title/Title';
import Header from '../../../Header/Header';
import Todo from 'components/Todo';
import BackBtn from '../../../BackBtn/BackBtn';

const todos = [
  {
    name: 'Смена пароля',
    link: '/',
    className: ''
  },
  {
    name: 'Заблокировать карту',
    link: '/',
    className: ''
  },
  {
    name: 'Тарифы и карты',
    link: '/',
    className: ''
  },
  {
    name: 'Тарифы и карты',
    link: '/',
    className: 'hidden'
  }
];

function TableItem() {
  const items = general.map(({ title, value }) => (
    <div className={Styles.item}>
      <div className={Styles.itemTitle}>{title}</div>
      <div className={Styles.itemValue}>{value}</div>
    </div>
  ));
  return <div className={Styles.table}>{items}</div>;
}

const Details = () => (
  <section className={Styles.details}>
    <Header />
    <div className={StylesIndex.container}>
      <div className={StylesIndex.row}>
        <Title title="Детали карты" />

        <BackBtn />

        <div className={Styles.settings}>
          <div className={Styles.card}>
            <div className={Styles.name}>Карта Универсальная</div>
            <div className={Styles.image}>
              <img src={card} alt="" />
            </div>
          </div>
          <div className={Styles.info}>
            <div className={Styles.amount}>
              17 703 <span className={Styles.coins}>00</span>₴
            </div>

            <Todo todos={todos} />
          </div>
        </div>
        <div className={Styles.general}>
          <h3 className={Styles.name}>Основное</h3>
          <TableItem />
        </div>
      </div>
    </div>
  </section>
);

export default Details;
