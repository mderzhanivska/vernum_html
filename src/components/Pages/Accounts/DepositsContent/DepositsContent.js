import React from 'react';
import { Link } from 'react-router-dom';

import Deposit from 'images/deposit_big.png';
import Styles from './DepositsContent.module.scss';
import deposits from './data';

function ListDeposits() {
  const listDeposits = deposits.map(({ name, amount, details }) => (
    <div className={Styles.deposit}>
      <div className={Styles.name} contentEditable>
        {name}
      </div>
      <img src={Deposit} className={Styles.image} alt="credit card" />
      <div className={Styles.amount}>{amount} ₴</div>
      <Link to={details} className={Styles.more}>
        Подробнее
      </Link>
    </div>
  ));
  return <div className={Styles.myDeposits}>{listDeposits}</div>;
}

const DepositsContent = () => <ListDeposits />;

export default DepositsContent;
