import BlackCard from 'images/card-black.png';
import WhiteCard from 'images/card-white.png';

export default [
  {
    name: 'Карта Универсальная',
    icon: WhiteCard,
    amount: '17 703, 00',
    details: '/accounts/details',
    owner: 'Иванов Иван',
    number: '5152 1551 5151 5152'
  },
  {
    name: 'Зарплатная',
    icon: BlackCard,
    amount: '17 703, 00',
    details: '/accounts/details',
    owner: 'Иванов Иван',
    number: '5152 1551 5151 5152'
  },
  {
    name: 'Накопительная',
    icon: WhiteCard,
    amount: '17 703, 00',
    details: '/accounts/details',
    owner: 'Иванов Иван',
    number: '5152 1551 5151 5152'
  },
  {
    name: 'Карта Универсальная',
    icon: BlackCard,
    amount: '17 703, 00',
    details: '/accounts/details',
    owner: 'Иванов Иван',
    number: '5152 1551 5151 5152'
  }
];
