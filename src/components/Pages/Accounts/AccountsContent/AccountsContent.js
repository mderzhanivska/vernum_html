import React from 'react';
import { Link } from 'react-router-dom';

import Styles from './AccountsContent.module.scss';
import cards from './data';

function ListCards() {
  const listCards = cards.map(({ icon, name, amount, details }) => (
    <div className={Styles.card}>
      <div className={Styles.name}>{name}</div>
      <img src={icon} className={Styles.image} alt="credit card" />
      <div className={Styles.amount}>{amount} ₴</div>
      <Link to={details} className={Styles.more}>
        Подробнее
      </Link>
    </div>
  ));
  return <div className={Styles.myCards}>{listCards}</div>;
}

const AccountsContent = () => <ListCards />;

export default AccountsContent;
