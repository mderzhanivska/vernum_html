import React from 'react';
import { Link } from 'react-router-dom';

import Credit from 'images/credit_big.png';
import Styles from './CreditsContent.module.scss';
import credits from './data';

function ListCredits() {
  const listCredits = credits.map(({ name, amount, details }) => (
    <div className={Styles.credit}>
      <div className={Styles.name}>{name}</div>
      <img src={Credit} className={Styles.image} alt="credit card" />
      <div className={Styles.amount}>{amount} ₴</div>
      <Link to={details} className={Styles.more}>
        Подробнее
      </Link>
    </div>
  ));
  return <div className={Styles.myCredits}>{listCredits}</div>;
}

const CreditsContent = () => <ListCredits />;

export default CreditsContent;
