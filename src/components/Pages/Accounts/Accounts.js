import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import StylesIndex from 'styles/index.module.scss';
import Styles from './Accounts.module.scss';
import AccountsContent from './AccountsContent/AccountsContent';
import DepositsContent from './DepositsContent/DepositsContent';
import CreditsContent from './CreditsContent/CreditsContent';
import Burger from '../../Navigation/Burger/Burger';
import Title from '../../Title/Title';

const Accounts = () => (
  <section className={Styles.accounts}>
    <div className={StylesIndex.container}>
      <div className={StylesIndex.row}>
        <Title title="Счета" />
        <Tabs className={Styles.tabs}>
          <TabList className={Styles.tablist}>
            <Tab className={Styles.tabitem}>Мои карты</Tab>
            <Tab className={Styles.tabitem}>Депозиты</Tab>
            <Tab className={Styles.tabitem}>Кредиты</Tab>
            <Tab className={Styles.tabitem}>Бизнес</Tab>
          </TabList>

          <TabPanel>
            <AccountsContent />
          </TabPanel>
          <TabPanel>
            <DepositsContent />
          </TabPanel>
          <TabPanel>
            <CreditsContent />
          </TabPanel>
          <TabPanel>
            <AccountsContent />
          </TabPanel>
        </Tabs>
      </div>
    </div>
  </section>
);

export default Accounts;
