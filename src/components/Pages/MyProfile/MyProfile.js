import React from 'react';
import { Tab, TabList, TabPanel, Tabs } from 'react-tabs';

import StylesIndex from 'styles/index.module.scss';
import Styles from './MyProfile.module.scss';

import General from './General/General';
import Safety from './Safety/Safety';
import Title from '../../Title/Title';

const MyProfile = () => (
  <section className={Styles.my_profile}>
    <div className={StylesIndex.container}>
      <div className={StylesIndex.row}>
        <Title title="Мой профиль" />

        <Tabs className={Styles.tabs}>
          <TabList className={Styles.tablist}>
            <Tab className={Styles.tabitem}>Основное</Tab>
            <Tab className={Styles.tabitem}>Безопасность</Tab>
          </TabList>

          <TabPanel>
            <General />
          </TabPanel>
          <TabPanel>
            <Safety />
          </TabPanel>
        </Tabs>
      </div>
    </div>
  </section>
);

export default MyProfile;
