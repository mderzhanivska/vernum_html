import React from 'react';
import Styles from './Search.module.scss';

import Loupe from 'images/loupe.png';

const Search = ({ label }) => (
  <div className={Styles.search}>
    <input type="search" placeholder={label} />
    <span className={Styles.loupe}>
      <img src={Loupe} alt="" />
    </span>
  </div>
);

export default Search;
