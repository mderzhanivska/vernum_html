import React from 'react';
import Letter from 'images/svg/letter.svg';
import Styles from './MessageBtn.module.scss';

const MessageBtn = () => (
  <div className={Styles.messageBtn}>
    <button className={Styles.btn}>
      <Letter />
    </button>
  </div>
);

export default MessageBtn;
