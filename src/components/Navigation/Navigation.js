import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Logout from 'images/svg/logout.svg';
import User from 'images/user.png';
import minLogo from 'images/logo_vernum.png';
import StylesIndex from 'styles/index.module.scss';
import Styles from './Navigation.module.scss';

import links from './data';

const NavList = () => (
  <ul className={Styles.list}>
    {links.map(({ icon: Icon, name, link }) => (
      <li className={Styles.listItem} key={name}>
        <Link to={link} className={Styles.link}>
          <div className={Styles.svg}>
            <Icon className={Styles.icon} />
          </div>
          <div className={Styles.name}>{name}</div>
        </Link>
      </li>
    ))}
  </ul>
);

class Navigation extends Component {
  render() {
    return (
      <section className={Styles.navigation}>
        <div className={StylesIndex.container}>
          <div className={StylesIndex.row}>
            <div className={Styles.logo}>
              <Link to="/">
                <img
                  className={Styles.logo_img}
                  src={minLogo}
                  alt="vernum-bank logo"
                />
              </Link>
            </div>

            <div className={Styles.user}>
              <img src={User} alt="user img" className={Styles.user_img} />
              <span className={Styles.title}>user</span>
              <Link to="/" className={Styles.link}>
                Настройки
              </Link>
            </div>

            <nav className={Styles.nav}>
              <NavList />
            </nav>

            <div className={Styles.signOut}>
              <Link to="/auth" className={Styles.link}>
                <Logout />
                <div className={Styles.name}>Выход</div>
              </Link>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Navigation;
