import MyVernum from 'images/svg/nav/my_vernum.svg';
import Atm from 'images/svg/nav/atm.svg';
import Cards from 'images/svg/nav/cards.svg';
import Chats from 'images/svg/nav/chats.svg';
import Currency from 'images/svg/nav/currency.svg';
import History from 'images/svg/nav/history.svg';
import Payments from 'images/svg/nav/payments.svg';
import Profile from 'images/svg/nav/profile.svg';
import Sale from 'images/svg/nav/sale.svg';

export default [
  {
    name: 'Мой Vernum',
    icon: MyVernum,
    link: '/vernum'
  },
  {
    name: 'Мои деньги',
    icon: Cards,
    link: '/accounts'
  },
  {
    name: 'Платежи',
    icon: Payments,
    link: '/payments'
  },
  {
    name: 'События',
    icon: History,
    link: '/history'
  },
  {
    name: 'Мой профиль',
    icon: Profile,
    link: '/profile'
  },
  {
    name: 'Чат с банком',
    icon: Chats,
    link: '/chat'
  },
  {
    name: 'Акции',
    icon: Sale,
    link: '/sales'
  },
  {
    name: 'Отделения и банкоматы',
    icon: Atm,
    link: '/atm'
  },
  {
    name: 'Курс валют',
    icon: Currency,
    link: '/currency'
  }
];
